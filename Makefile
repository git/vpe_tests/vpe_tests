all:	testvpem2m

CC := $(CROSS_COMPILE)gcc
DESTDIR ?= .

#kernel uapi path
#KDIR ?= $(HOME)/linux/include

CFLAGS=-g -Wall -I$(KDIR)/uapi -I$(KDIR)


testvpem2m:	vpetest.o
	$(CC) $(CFLAGS) vpetest.o -o testvpem2m

main.o:	vpetest.c
	$(CC) $(CFLAGS)  vpetest.c

install: testvpem2m
	 install -d $(DESTDIR)/usr/bin
	 install -d $(DESTDIR)/lib/firmware
	 install testvpem2m $(DESTDIR)/usr/bin
	 install vpdma-1b8.bin $(DESTDIR)/lib/firmware

clean:
	rm -f *.o *~ testvpem2m *.opt
