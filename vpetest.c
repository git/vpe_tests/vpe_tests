// SPDX-License-Identifier: GPL-2.0
/*
 *  Copyright (c) 2012-2013, Texas Instruments Incorporated  -  http://www.ti.com/
 */

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <getopt.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>

#include <linux/videodev2.h>
#include <linux/v4l2-controls.h>

#include <sys/mman.h>
#include <sys/time.h>
#include <sys/ioctl.h>

static int debug = 0;
#define dprintf(fmt, arg...) if (debug) {printf(fmt, ## arg); fflush(stdout);}

#define pexit(str) { \
	perror(str); \
	exit(1); \
}

#define V4L2_CID_TRANS_NUM_BUFS         (V4L2_CID_USER_TI_VPE_BASE + 0)

/**<	Input  file descriptor				*/
int	fin = -1;

/**<	Output file descriptor				*/
int	fout = -1;

/**<	File descriptor for device			*/
int	fd   = -1;

#define tCapture V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE
#define tOutput V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE

int describeFormat(char *format, int width, int height, int *size, int *fourcc,
		   int *num_planes, enum v4l2_colorspace *clrspc)
{
	*size   = -1;
	*fourcc = -1;
	if (strcmp(format, "rgb24") == 0) {
		*fourcc = V4L2_PIX_FMT_RGB24;
		*size = height * width * 3;
		*num_planes = 1;
		*clrspc = V4L2_COLORSPACE_SRGB;

	} else if (strcmp(format, "bgr24") == 0) {
		*fourcc = V4L2_PIX_FMT_BGR24;
		*size = height * width * 3;
		*num_planes = 1;
		*clrspc = V4L2_COLORSPACE_SRGB;

	} else if (strcmp(format, "argb32") == 0) {
		*fourcc = V4L2_PIX_FMT_RGB32;
		*size = height * width * 4;
		*num_planes = 1;
		*clrspc = V4L2_COLORSPACE_SRGB;

	} else if (strcmp(format, "abgr32") == 0) {
		*fourcc = V4L2_PIX_FMT_BGR32;
		*size = height * width * 4;
		*num_planes = 1;
		*clrspc = V4L2_COLORSPACE_SRGB;

	} else if (strcmp(format, "rgb565") == 0) {
		*fourcc = V4L2_PIX_FMT_RGB565;
		*size = height * width * 2;
		*num_planes = 1;
		*clrspc = V4L2_COLORSPACE_SRGB;

	} else if (strcmp(format, "yuv444") == 0) {
		*fourcc = V4L2_PIX_FMT_YUV444;
		*size = height * width * 3;
		*num_planes = 1;
		*clrspc = V4L2_COLORSPACE_SMPTE170M;

	} else if (strcmp(format, "yvyu") == 0) {
		*fourcc = V4L2_PIX_FMT_YVYU;
		*size = height * width * 2;
		*num_planes = 1;
		*clrspc = V4L2_COLORSPACE_SMPTE170M;

	} else if (strcmp(format, "yuyv") == 0) {
		*fourcc = V4L2_PIX_FMT_YUYV;
		*size = height * width * 2;
		*num_planes = 1;
		*clrspc = V4L2_COLORSPACE_SMPTE170M;

	} else if (strcmp(format, "uyvy") == 0) {
		*fourcc = V4L2_PIX_FMT_UYVY;
		*size = height * width * 2;
		*num_planes = 1;
		*clrspc = V4L2_COLORSPACE_SMPTE170M;

	} else if (strcmp(format, "vyuy") == 0) {
		*fourcc = V4L2_PIX_FMT_VYUY;
		*size = height * width * 2;
		*num_planes = 1;
		*clrspc = V4L2_COLORSPACE_SMPTE170M;

	} else if (strcmp(format, "nv16") == 0) {
		*fourcc = V4L2_PIX_FMT_NV16;
		*size = height * width * 2;
		*num_planes = 1;
		*clrspc = V4L2_COLORSPACE_SMPTE170M;

	} else if (strcmp(format, "nv61") == 0) {
		*fourcc = V4L2_PIX_FMT_NV61;
		*size = height * width * 2;
		*num_planes = 1;
		*clrspc = V4L2_COLORSPACE_SMPTE170M;

	} else if (strcmp(format, "nv12") == 0) {
		*fourcc = V4L2_PIX_FMT_NV12;
		*size = height * width * 1.5;
		*num_planes = 1;
		*clrspc = V4L2_COLORSPACE_SMPTE170M;

	} else if (strcmp(format, "nv21") == 0) {
		*fourcc = V4L2_PIX_FMT_NV21;
		*size = height * width * 1.5;
		*num_planes = 1;
		*clrspc = V4L2_COLORSPACE_SMPTE170M;

	} else if (strcmp(format, "nm12") == 0) {
		*fourcc = V4L2_PIX_FMT_NV12M;
		*size = height * width * 1.5;
		*num_planes = 2;
		*clrspc = V4L2_COLORSPACE_SMPTE170M;

	} else if (strcmp(format, "nm21") == 0) {
		*fourcc = V4L2_PIX_FMT_NV21M;
		*size = height * width * 1.5;
		*num_planes = 2;
		*clrspc = V4L2_COLORSPACE_SMPTE170M;

	} else {
		return 0;
	}

	return 1;
}

int allocBuffers(int type, int width, int height, int num_planes,
		 enum v4l2_colorspace clrspc, int *sizeimage_y,
		 int *sizeimage_uv, int fourcc, void *base[],
		 void *base_uv[], int *numbuf, int interlace,
		 struct v4l2_selection s)
{
	struct v4l2_format fmt;
	struct v4l2_requestbuffers reqbuf;
	struct v4l2_buffer buffer;
	struct v4l2_plane buf_planes[2];
	int i = 0;
	int ret = -1;

	bzero(&fmt, sizeof(fmt));
	fmt.type = type;
	fmt.fmt.pix_mp.width = width;
	fmt.fmt.pix_mp.height = height;
	fmt.fmt.pix_mp.pixelformat = fourcc;
	fmt.fmt.pix_mp.colorspace = clrspc;
	fmt.fmt.pix_mp.num_planes = num_planes;

	if (type == tOutput && interlace == 1)
		fmt.fmt.pix_mp.field = V4L2_FIELD_ALTERNATE;
	else if (type == tOutput && interlace == 2)
		fmt.fmt.pix_mp.field = V4L2_FIELD_SEQ_TB;
	else
		fmt.fmt.pix_mp.field = V4L2_FIELD_ANY;

	ret = ioctl(fd, VIDIOC_S_FMT, &fmt);
	if (ret < 0) {
		pexit("Cant set color format\n");
	} else {
		*sizeimage_y = fmt.fmt.pix_mp.plane_fmt[0].sizeimage;
		*sizeimage_uv = fmt.fmt.pix_mp.plane_fmt[1].sizeimage;
	}

	if (s.type == V4L2_BUF_TYPE_VIDEO_OUTPUT) {
		ret = ioctl(fd, VIDIOC_S_SELECTION, &s);
		if (ret < 0)
			pexit("error setting selection\n");
	}

	dprintf("Buffer Type:%s, requested %dx%d got %dx%d\n",
		V4L2_TYPE_IS_OUTPUT(type)?"output":"capture",
		width, height, fmt.fmt.pix_mp.width, fmt.fmt.pix_mp.height);

	bzero(&reqbuf, sizeof(reqbuf));
	reqbuf.count = *numbuf;
	reqbuf.type = type;
	reqbuf.memory = V4L2_MEMORY_MMAP;

	ret = ioctl(fd, VIDIOC_REQBUFS, &reqbuf);
	if (ret < 0) {
		pexit("Cant request buffers\n");
	} else {
		*numbuf = reqbuf.count;
	}

	for (i = 0; i < *numbuf; i++) {
		memset(&buffer, 0, sizeof(buffer));
		buffer.type = type;
		buffer.memory = V4L2_MEMORY_MMAP;
		buffer.index = i;
		buffer.m.planes	= buf_planes;
		buffer.length = num_planes;

		ret = ioctl(fd, VIDIOC_QUERYBUF, &buffer);
		if (ret < 0)
			pexit("Cant query buffers\n");

		printf("query buf, plane 0 = %d\n",
		       buffer.m.planes[0].length);
		if (num_planes == 2)
			printf("           plane 1 = %d\n",
			       buffer.m.planes[1].length);

		base[i]	= mmap(NULL, buffer.m.planes[0].length,
				PROT_READ | PROT_WRITE, MAP_SHARED,
				fd, buffer.m.planes[0].m.mem_offset);

		if (MAP_FAILED == base[i]) {
			while (i >= 0) {
				/* Unmap all previous buffers */
				i--;
				munmap(base[i], *sizeimage_y);
				base[i] = NULL;
			}
			pexit("Cant mmap buffers Y");
			return 0;
		}

		if (num_planes == 1)
			continue;

		base_uv[i] = mmap(NULL, buffer.m.planes[1].length,
				  PROT_READ | PROT_WRITE, MAP_SHARED,
				  fd, buffer.m.planes[1].m.mem_offset);

		if (MAP_FAILED == base_uv[i]) {
			while (i >= 0) {
				/* Unmap all previous buffers */
				i--;
				munmap(base_uv[i], *sizeimage_uv);
				base[i] = NULL;
			}
			pexit("Cant mmap buffers UV");
			return 0;
		}
	}

	return 1;
}

void releaseBuffers(void *base[], int numbuf, int bufsize)
{
	while (numbuf > 0) {
		numbuf--;
		munmap(base[numbuf], bufsize);
	}
}

int queue(int type, int index, int field, int size_y, int size_uv)
{
	struct v4l2_buffer buffer;
	struct v4l2_plane buf_planes[2];

	dprintf("DBG queue(%d, %d, %d, %d, %d)\n",
		type, index, field, size_y, size_uv);
	buf_planes[0].length = buf_planes[0].bytesused = size_y;
	buf_planes[1].length = buf_planes[1].bytesused = size_uv;
	buf_planes[0].data_offset = buf_planes[1].data_offset = 0;

	memset(&buffer, 0, sizeof(buffer));
	buffer.type = type;
	buffer.memory = V4L2_MEMORY_MMAP;
	buffer.index = index;
	buffer.m.planes	= buf_planes;
	buffer.field = field;
	buffer.length = 2;

	return ioctl(fd, VIDIOC_QBUF, &buffer);
}

int queue_all(int type, int numbuf, int field, int size_y, int size_uv)
{
	int i = 0;
	int ret = -1;
	int lastqueued = -1;

	for (i = 0; i < numbuf; i++) {
		ret = queue(type, i, field, size_y, size_uv);
		if (-1 == ret)
			break;
		else
			lastqueued = i;
	}
	return lastqueued;
}

int dequeue(int	type, struct v4l2_buffer *buf, struct v4l2_plane *buf_planes)
{
	memset(buf, 0, sizeof(*buf));
	buf->type = type;
	buf->memory = V4L2_MEMORY_MMAP;
	buf->m.planes = buf_planes;
	buf->length = 2;
	return ioctl(fd, VIDIOC_DQBUF, buf);
}

void streamON(int type)
{
	int ret = -1;
	ret = ioctl(fd, VIDIOC_STREAMON, &type);
	if (-1 == ret)
		pexit("Cant Stream on\n");
}

void streamOFF(int type)
{
	int ret = -1;
	ret = ioctl(fd, VIDIOC_STREAMOFF, &type);
	if (-1 == ret)
		pexit("Cant Stream on\n");
}

void do_read(char *str, int fd, void *addr, int size)
{
	int nbytes = size, ret = 0, val;
	do {
		nbytes = size - ret;
		addr = addr + ret;
		if (nbytes == 0)
			break;
		ret = read(fd, addr, nbytes);
	} while (ret > 0);

	if (ret < 0) {
		val = errno;
		printf("Reading failed %s: %d %s\n", str, ret, strerror(val));
		exit(1);
	} else {
		dprintf("Total bytes read %s = %d\n", str, size);
	}
}

void do_write(char *str, int fd, void *addr, int size)
{
	int nbytes = size, ret = 0, val;
	do {
		nbytes = size - ret;
		addr = addr + ret;
		if (nbytes == 0)
			break;
		ret = write(fd, addr, nbytes);
	} while (ret > 0);
	if (ret < 0) {
		val = errno;
		printf("Writing failed %s: %d %s\n", str, ret, strerror(val));
		exit(1);
	} else {
		dprintf("Total bytes written %s = %d\n", str, size);
	}
}

static void usage(void)
{
	char localusage[] =
	"Usage: vpetest [-d <Device file|number>] -i <Input> -j <WxH> -k <Pixel Format>\n"
	"-o <Output> -p <WxH> -q <Pixel Format>\n"
	"[-c <top,left,width,height>] [-l [0-2] [-t [1-4]]\n"
	"Convert input video file into desired output format\n"
	"\t[-d <Device file>] : /dev/video0 (default)\n"
	"\t-i <Input>         : Input file name\n"
	"\t-j <WxH>           : Input frame size\n"
	"\t-k <Pixel Format>  : Input frame format\n"
	"\t-o <Output>        : Output file name\n"
	"\t-p <WxH>           : Output frame size\n"
	"\t-q <Pixel Format>  : Output frame format\n"
	"\t[-c <top,left,width,height>]  : Crop target\n"
	"\t[-l [0-2]]         : 0=no deinterlacing (default), 1=alternate field, 2=seq_tb\n"
	"\t[-t <1-4>]         : Number of buffers\n"
	"\t[-v]               : Verbose/Debug\n";

	printf("%s\n", localusage);
}

int main(int argc, char *argv[])
{
	char shortoptions[] = "d:i:j:k:o:p:q:c:l:t:v";
	int c, ret = 0;
	int index;
	int i;
	char devname[30];
	char srcfile[256];
	char dstfile[256];
	int srcHeight = 0, dstHeight = 0;
	int srcWidth = 0, dstWidth = 0;
	int srcSize = 0, dstSize = 0, srcSize_uv = 0, dstSize_uv = 0;
	char srcFmt[30], dstFmt[30];
	int srcFourcc = 0, dstFourcc = 0;
	int src_num_planes = 1, dst_num_planes = 1;
	enum v4l2_colorspace src_colorspace, dst_colorspace;

	void *srcBuffers[6];
	void *dstBuffers[6];
	void *srcBuffers_uv[6];
	void *dstBuffers_uv[6];
	int src_numbuf = 6;
	int dst_numbuf = 6;
	int num_frames = 20;
	int interlace = 0;
	int translen = 3;
	int frame_no = 0;
	struct v4l2_control ctrl;
	struct v4l2_selection selection;
	struct timeval now;
	int latency;
	int field;
	off_t file_size;

	/* let's setup default values before parsing arguments */
	strcpy(devname, "/dev/video0");
	srcfile[0] = '\0';
	dstfile[0] = '\0';

	selection.r.top = selection.r.left = 0;
	selection.r.width = 0;
	selection.r.height = 0;
	selection.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
	selection.target = V4L2_SEL_TGT_CROP_ACTIVE;
	interlace = 0;
	translen = 3;

	for (;;) {
		char *endptr;

		c = getopt_long(argc, argv, shortoptions, (void *)NULL, &index);
		if (-1 == c)
			break;
		switch (c) {
		case 0:
			break;
		case 'd':
		case 'D':
                        if (isdigit(optarg[0]) && strlen(optarg) <= 3) {
                                sprintf(devname, "/dev/video%s", optarg);
                        } else if (!strncmp(optarg, "/dev/video", 10)) {
				strcpy(devname, optarg);
			} else {
				printf("ERROR: Device name not recognized: %s\n\n",
				       optarg);
				usage();
				exit(1);
			}
			printf("device_name: %s\n", devname);
			break;
		case 'i':
		case 'I':
			strcpy(srcfile, optarg);
			printf("srcfile: %s\n", srcfile);
			break;
		case 'j':
		case 'J':
			srcWidth = strtol(optarg, &endptr, 10);
			if (*endptr != 'x' || endptr == optarg) {
				printf("Invalid size '%s'\n", optarg);
				return 1;
			}
			srcHeight = strtol(endptr + 1, &endptr, 10);
			if (*endptr != 0) {
				printf("Invalid size '%s'\n", optarg);
				return 1;
			}
			/* default crop values at first */
			if (selection.r.height == 0) {
				selection.r.top = selection.r.left = 0;
				selection.r.width = srcWidth;
				selection.r.height = srcHeight;
			}
			break;
		case 'k':
		case 'K':
			strcpy(srcFmt, optarg);
			break;
		case 'o':
		case 'O':
			strcpy(dstfile, optarg);
			printf("dstfile: %s\n", dstfile);
			break;
		case 'p':
		case 'P':
			dstWidth = strtol(optarg, &endptr, 10);
			if (*endptr != 'x' || endptr == optarg) {
				printf("Invalid size '%s'\n", optarg);
				return 1;
			}
			dstHeight = strtol(endptr + 1, &endptr, 10);
			if (*endptr != 0) {
				printf("Invalid size '%s'\n", optarg);
				return 1;
			}
			break;
		case 'q':
		case 'Q':
			strcpy(dstFmt, optarg);
			break;
		case 'c':
		case 'C':
			selection.r.top = strtol(optarg, &endptr, 10);
			if (*endptr != ',' || endptr == optarg) {
				printf("Invalid size '%s'\n", optarg);
				return 1;
			}
			selection.r.left = strtol(endptr + 1, &endptr, 10);
			if (*endptr != ',' || endptr == optarg) {
				printf("Invalid size '%s'\n", optarg);
				return 1;
			}
			selection.r.width = strtol(endptr + 1, &endptr, 10);
			if (*endptr != ',' || endptr == optarg) {
				printf("Invalid size '%s'\n", optarg);
				return 1;
			}
			selection.r.height = strtol(endptr + 1, &endptr, 10);
			if (*endptr != 0) {
				printf("Invalid size '%s'\n", optarg);
				return 1;
			}
			break;
		case 'l':
		case 'L':
			interlace = atoi(optarg);
			break;
		case 't':
		case 'T':
			translen = atoi(optarg);
			break;
		case 'v':
		case 'V':
			debug = 1;
			break;
		default:
			usage();
			exit(1);
		}
	}

	/** Open input file in read only mode */
	fin = open(srcfile, O_RDONLY);
	describeFormat(srcFmt, srcWidth, srcHeight, &srcSize,
		       &srcFourcc, &src_num_planes, &src_colorspace);

	/** Open output file in write mode Create the file if not present */
	fout = open(dstfile, O_WRONLY | O_CREAT | O_TRUNC, 0666);
	describeFormat(dstFmt, dstWidth, dstHeight, &dstSize,
		       &dstFourcc, &dst_num_planes, &dst_colorspace);

	/** Calculate number of frames **/
	file_size = lseek(fin, 0L, SEEK_END);
	num_frames = file_size / srcSize;
	lseek(fin, 0L, SEEK_SET);


	printf("Input  @ %d = %d x %d , %d\nOutput @ %d = %d x %d , %d\n",
	       fin,  srcWidth, srcHeight, srcFourcc,
	       fout, dstWidth, dstHeight, dstFourcc);

	printf("Crop/Selection top: %d left: %d width %d height: %d\n",
	       selection.r.top, selection.r.left,
	       selection.r.width, selection.r.height);

	printf("Interlace: %d\n", interlace);

	/* SEQ_TB mode is 2 field per buffers so total number of frames will double */
	if (interlace == 2)
		num_frames *= 2;

	printf("Number of frames = %d\n", num_frames);

	if (fin  < 0 || srcHeight < 0 || srcWidth < 0 || srcFourcc < 0 ||
	    fout < 0 || dstHeight < 0 || dstWidth < 0 || dstFourcc < 0) {
		/** TODO:Handle errors precisely		*/
		exit(1);
	}
	/*************************************
		Files are ready Now
	*************************************/

	fd = open(devname, O_RDWR);
	if (fd < 0)
		pexit("Cannot open device\n");

	memset(&ctrl, 0, sizeof(ctrl));
	ctrl.id = V4L2_CID_TRANS_NUM_BUFS;
	ctrl.value = translen;
	ret = ioctl(fd, VIDIOC_S_CTRL, &ctrl);
	if (ret < 0)
		printf("Can't set translen control, skipping...\n");

	ret = allocBuffers(tOutput, srcWidth, srcHeight, src_num_planes,
			   src_colorspace, &srcSize, &srcSize_uv, srcFourcc,
			   srcBuffers, srcBuffers_uv, &src_numbuf, interlace,
			   selection);
	if (ret < 0)
		pexit("Cant Allocate buffurs for OUTPUT device\n");

	ret = allocBuffers(tCapture, dstWidth, dstHeight, dst_num_planes,
			   dst_colorspace, &dstSize, &dstSize_uv, dstFourcc,
			   dstBuffers, dstBuffers_uv, &dst_numbuf, interlace,
			   selection);
	if (ret < 0)
		pexit("Cant Allocate buffurs for CAPTURE device\n");

	/**	Queue All empty buffers	(Available to capture in)	*/
	ret = queue_all(tCapture, dst_numbuf, V4L2_FIELD_ANY, dstSize, dstSize_uv);
	if (ret < 0)
		pexit("Error queueing buffers for CAPTURE device\n");

	printf("Input  Buffers = %d each of size %d\nOutput Buffers = %d each of size %d\n",
	       src_numbuf, srcSize, dst_numbuf, dstSize);

	/*************************************
		Driver is ready Now
	*************************************/

	/**	Read  into the OUTPUT  buffers from fin file	*/

	switch (interlace) {
	case 2:
		field = V4L2_FIELD_SEQ_TB;
		break;
	case 1:
		field = V4L2_FIELD_TOP;
		break;
	default:
		field = V4L2_FIELD_ANY;
		break;
	}
	for (i = 0; i < src_numbuf && i < num_frames; i++) {
		do_read("Y plane", fin, srcBuffers[i], srcSize);
		if (src_num_planes == 2)
			do_read("UV plane", fin, srcBuffers_uv[i], srcSize_uv);

		queue(tOutput, i, field, srcSize, srcSize_uv);
		if (field == V4L2_FIELD_TOP)
			field = V4L2_FIELD_BOTTOM;
		else if (field == V4L2_FIELD_BOTTOM)
			field = V4L2_FIELD_TOP;
	}

	/*************************************
		Data is ready Now
	*************************************/

	streamON(tOutput);
	streamON(tCapture);

	if (!debug) {
		printf("frames left %04d", num_frames);
		fflush(stdout);
	}
	while (num_frames) {
		struct v4l2_buffer buf;
		struct v4l2_plane buf_planes[2];
		int last = num_frames == 1 ? 1 : 0;
		int iter = interlace == 2 ? 2 : 1;

		/* DEI: Do not Dequeue Source buffers immediately
		 * De*interlacer keeps last two buffers in use */
		if (!(interlace && frame_no <= 2)) {
			/* Wait for and dequeue one buffer from OUTPUT
			 * to write data for next interlaced field */
			dequeue(tOutput, &buf, buf_planes);
			dprintf("dequeued source buffer with index %d\n",
				buf.index);

			if (!last) {
				do_read("Y plane", fin, srcBuffers[buf.index],
					srcSize);
				if (src_num_planes == 2)
					do_read("UV plane", fin,
						srcBuffers_uv[buf.index],
						srcSize_uv);

				queue(tOutput, buf.index, field, srcSize,
				      srcSize_uv);
				if (field == V4L2_FIELD_TOP)
					field = V4L2_FIELD_BOTTOM;
				else if (field == V4L2_FIELD_BOTTOM)
					field = V4L2_FIELD_TOP;
			}
		}

		while (iter--) {
			/* Dequeue progressive frame from CAPTURE stream
			 * write to the file and queue one empty buffer */
			dequeue(tCapture, &buf, buf_planes);
			dprintf("dequeued dest buffer with index %d\n",
				buf.index);

			gettimeofday(&now, NULL);
			latency = now.tv_usec - buf.timestamp.tv_usec;
			if (latency < 0)
				latency += 1000000;
			latency += (now.tv_sec - buf.timestamp.tv_sec) *
				   1000000;
			dprintf("Latency = %7d us\n", latency);

			do_write("Y plane", fout, dstBuffers[buf.index],
				 dstSize);
			if (dst_num_planes == 2)
				do_write("UV plane", fout,
					 dstBuffers_uv[buf.index], dstSize_uv);

			if (!last)
				queue(tCapture, buf.index, V4L2_FIELD_NONE,
				      dstSize, dstSize_uv);

			num_frames--;
			frame_no++;

			if (debug) {
				printf("frames left %04d\n", num_frames);
				fflush(stdout);
			} else {
				printf("\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\bframes left %04d", num_frames);
				fflush(stdout);
			}
		}
	}

	printf("\n");

	/* Driver cleanup */
	streamOFF(tOutput);
	streamOFF(tCapture);

	releaseBuffers(srcBuffers, src_numbuf, srcSize);
	releaseBuffers(dstBuffers, dst_numbuf, dstSize);

	if (src_num_planes == 2)
		releaseBuffers(srcBuffers_uv, src_numbuf, srcSize_uv);
	if (dst_num_planes == 2)
		releaseBuffers(dstBuffers_uv, dst_numbuf, dstSize_uv);

	close(fin);
	close(fout);
	close(fd);

	return 0;
}

